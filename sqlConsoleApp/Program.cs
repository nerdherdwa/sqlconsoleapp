﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;


namespace sqlConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            string sqlConn = ConfigurationManager.ConnectionStrings["sqlConn"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(sqlConn))
            {
                            
                conn.Open();

                SqlCommand cmd = new SqlCommand("SELECT   ProductNumber , Name, [Price Range] =CASE WHEN ListPrice = 0 THEN 'Not for sale' WHEN ListPrice < 50 THEN 'Under $50'  WHEN ListPrice >= 50 and ListPrice < 250 THEN 'Under $250' WHEN ListPrice >= 250 and ListPrice < 1000 THEN 'Under $1000' ELSE 'Over $1000' END FROM Production.Product ORDER BY ProductNumber", conn);

                using(SqlDataReader rdr = cmd.ExecuteReader())
                {
                    Console.WriteLine("Product Number\t\tName\t\tPrice Range\t");
                    while (rdr.Read())
                    {
                        Console.WriteLine(String.Format("{0}\t | {1} \t | {2}", rdr[0], rdr[1], rdr[2]));
                    }
                }

                Console.WriteLine("Data Displayed! Press enter to continue...");
                Console.ReadLine();
                Console.Clear();

            }
            using (productNumberDataContext pn = new productNumberDataContext())
                {
                    var query = from a in pn.Products
                                where a.ListPrice >= 50 && a.ListPrice < 250
                                orderby a.ProductNumber
                                select new
                                {
                                    productNumber = a.ProductNumber,
                                    name = a.Name,
                                    priceRange =    a.ListPrice == 0 ? "Not for sale" :
                                                    a.ListPrice < 50 ? "Under $50" :
                                                    a.ListPrice >= 50 && a.ListPrice < 250 ? "Under $250" :
                                                    a.ListPrice >= 250 && a.ListPrice < 1000 ? "Under $1000" :
                                                    a.ListPrice >= 1000 ? "Over 1000" : ""
                                };

                    foreach (var q in query)

                    {
                        Console.WriteLine("{0}\t\t\t | {1}\t\t | {2}", q.productNumber, q.name, q.priceRange);
                    }

                    Console.WriteLine("Data Displayed! Press enter to continue...");
                    Console.ReadLine();
                    Console.Clear();
                }

            
        }
    }
}
